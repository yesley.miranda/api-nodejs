const Logger = require("../utils/logger");

class Methods {

  LoggerAndResponse(filename, req, res, error) {
    const logger = new Logger(filename);
    logger.error(error.stack || JSON.stringify(error));
    res.status(error.status || 500).json(req.body_response.setError(error));
  }
}

module.exports = new Methods();

/* eslint-disable no-undef */
const log4js = require("log4js");

const level = process.env.NODE_ENV === "test" ? "off" : "info";
log4js.configure({
  appenders: {"api": {type: "console"}},
  categories: {default: {appenders: ["api"], level}}
});

const logger = log4js.getLogger(" ");

module.exports = class Logger {
  constructor(filename) {
    this.filename = filename;
  }

  info(...message) {
    logger.info(`[${this.replaceFileName()}]  ${message.join(" ")}`);
  }

  warning(...message) {
    logger.warn(`[${this.replaceFileName()}]  ${message.join(" ")}`);
  }

  error(...message) {
    logger.error(`[${this.replaceFileName()}]  ${message.join(" ")}`);
  }

  logger() {
    return logger;
  }

  log4js() {
    return log4js;
  }

  replaceFileName() {
    try {
      return this.filename.split("src/")[1];
    } catch (e) {
      return this.filename;
    }
  }
};

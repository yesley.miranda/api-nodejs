/* eslint-disable no-undef */

const fs = require("fs");
const sequelize = require("../database/database").sequelize;
const Sequelize = require("sequelize");
const path = require("path");
const basename = path.basename(__filename);

/**  Initialize models  **/
const db = {};

fs.readdirSync(__dirname)
    .filter(file => {
      return (
          file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach(file => {
      try {
        const EntityModel = require(`../models/${file}`);
        if (EntityModel.init) {
          EntityModel.init(sequelize);
          db[file] = EntityModel;

        }
      } catch (e) {
        console.log(path.join(__dirname, file), e.stack);
      }
    });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(sequelize.models);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;

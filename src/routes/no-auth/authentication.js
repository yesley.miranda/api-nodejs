const router = require("express").Router();

const {intercept} = require("../../validations");

const validations = require("../../validations/authentication");
const controller = require("../../controllers/authentication");

/** POST user authentication **/
router.post("/authentication", validations.validateAuth(), intercept, controller.authentication);

module.exports = router;

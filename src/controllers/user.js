"use strict";

const {isCreated, isUpdated, isFound, isRemoved} = require("../validations");
const {updateEntity, createEntity} = require("../repository");
const {LoggerAndResponse} = require("../utils/methods");

// eslint-disable-next-line no-undef
const _filename = __filename;

const User = require("../models/User");

class PostsController {

  /** Get all entities **/
  async getAll(req, res) {
    try {

      const users = await User.findAll({where: {removed: false}});

      req.body_response.setOk(users, res);

    } catch (e) {
      LoggerAndResponse(_filename, req, res, e);
    }
  }

  /** Post create new entity **/
  async create(req, res) {
    try {

      const {name, last_name, username, password} = req.body;

      await User.validateDuplicatedUsername(username);

      const entity = await createEntity(User, {name, last_name, username, password});

      isCreated(entity);

      req.body_response.setCreated(entity, res);

    } catch (e) {
      LoggerAndResponse(_filename, req, res, e);
    }

  }

  async update(req, res) {
    try {

      const {id} = req.params;

      const entity = await User.findOne({where: {id}});

      isFound(entity, "Usuário");

      if (entity.username !== req.body.username)
        await User.validateDuplicatedUsername(req.body.username);

      const update = updateEntity(User, req.body, {where: {id}});

      isUpdated(update);

      req.body_response.setOk(update, res);

    } catch (e) {
      LoggerAndResponse(_filename, req, res, e);
    }
  }

  async delete(req, res) {
    try {

      const {id} = req.params;

      const entity = await User.findOne({where: {id, removed : false}});

      isFound(entity, "Usuário");

      const remove = updateEntity(User, {removed: true}, {where: {id}});

      isRemoved(remove);

      req.body_response.setOk(remove, res);

    } catch (e) {
      LoggerAndResponse(_filename, req, res, e);
    }
  }

}

module.exports = new PostsController();

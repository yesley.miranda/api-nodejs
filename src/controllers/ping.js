"use strict";

const router = require("express").Router();

const {_PING} = require("../utils/messages");

/** Ping para verificar disponibilidade da API **/
router.get("/ping", (req, res) => {
  try {
    req.body_response.setMessage(_PING);
    req.body_response.setOk({ping: "pong"});
    res.status(200).json(req.body_response);
  } catch (e) {
    res.status(e.status || 500).json(req.body_response.setError(e));
  }
});

module.exports = router;

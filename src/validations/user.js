const {body, param} = require("express-validator");

class PostsValidations {

  validateNew() {
    return [
      body(["name"], "Tamanho inválido (1 à 50)").isLength({min: 1, max: 50}),
      body(["last_name", "username"], "Tamanho inválido (5 à 100)").isLength({min: 5, max: 100}),
      body(["password"], "Tamanho inválido (6 à 50)").isLength({min: 6, max: 500})
    ];
  }

  validateUpdate() {
    return [
      param(["id"], "required"),
      body(["name"], "Tamanho inválido (1 à 50)").isLength({min: 1, max: 50}),
      body(["last_name", "username"], "Tamanho inválido (5 à 100)").isLength({min: 5, max: 100}),
      body(["password"], "Tamanho inválido (6 à 50)").isLength({min: 6, max: 500})
    ];
  }
}

module.exports = new PostsValidations();
